﻿
var apiServer = 'https://query.yahooapis.com/v1/public/yql?';

$(document).ready(function () {
    var cityName =  'Vancouver';
    var queryString =
        'select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="' +
            cityName +
            '")';


    $.ajax({
        url: apiServer,
        data: {
            format: 'json',
            q: queryString,
        },
        success: OnWeatherComplete
    });
});


function OnWeatherComplete(data) {

    var day = new Date().getDay();
    var days = ["Sun","Mon","Tue","Wed","Thur","Fri","Sat"];

    //Todays Weather
    $('#forecast_0_icon').attr("src", "http://l.yimg.com/a/i/us/we/52/" + data.query.results.channel.item.condition.code + ".gif");
    $('#forecast_0_temp').text(ToCelsius(data.query.results.channel.item.condition.temp));
    $('#forecast_0_high').text('High:' + ToCelsius(data.query.results.channel.item.forecast[0].high));
    $('#forecast_0_low').text('Low:' + ToCelsius(data.query.results.channel.item.forecast[0].low));
    $('#forecast_0_sunrise').text('Sunrise:' + data.query.results.channel.astronomy.sunrise);
    $('#forecast_0_sunset').text('Sunset:' + data.query.results.channel.astronomy.sunset);
    $('#forecast_0_wind').text('Wind Speed:' + ToKM(data.query.results.channel.wind.speed));
    $('#forecast_0_text').text('Forecast: ' + data.query.results.channel.item.condition.text);

    //3Day Forecast
    for (var i = 1; i <= 3; i++) {
        var dayIndex = (day + i) % 7;
        $('#forecast_' + i + '_icon').attr("src", "http://l.yimg.com/a/i/us/we/52/" + data.query.results.channel.item.forecast[i].code + ".gif");
        $('#forecast_' + i).text(days[dayIndex]);
        $('#forecast_' + i + '_high').text('H:' + ToCelsius(data.query.results.channel.item.forecast[i].high));
        $('#forecast_' + i + '_low').text('L:' + ToCelsius(data.query.results.channel.item.forecast[i].low));
    }
}

function ToKM(miles) {
    var km = Math.round(miles * 1.60934);
    return km + ' km/h'
}

function ToCelsius(farenheit) {
    
    var celsius = Math.round((farenheit - 32) / 1.8);
    return celsius;
}

