$( document ).ready(function() {
    
    var $form = $('.form-typeaheadonerrorpage');
    var $input = $('.form-typeaheadonerrorpage #form-typeahead-input');
    var $close = $('.btn-close');

   
    $input.typeahead(
        {
        hint: true,
        highlight: true,
    } , {
        limit: 10,
        templates: {
           suggestion: function(data) {
                return '<p>' + data.Name + '</p>';
            }
        },
        display: function(item){ return item.Name},
        name: 'tours',
        source: function(query, syncResults, asyncResults) {
            $.get("/home/AutoCompleteSuggestions?maxrows=4&name_startsWith=" + query, function(data) {
              asyncResults(data);
            });
        }
    });

    $close.on("click", function (E) {

        $input.typeahead('close');
        $input.val('');
    });

    $form.on("submit", function() {

        if (this.city.value == "") {
            
            $($input).focus();
            return false;
        }

        return true;
    });

    $input.bind('typeahead:select', function(ev, suggestion) {
        
        window.location.href = window.location.origin  + suggestion.ClubInUrl;
    });
});