//var urlTeeTimes = "http://10.10.50.104:58067/Web/GolfService.svc/TeeTimes/@Model.ID/" + "@String.Format("{0: yyyy-MM-dd}", DateTime.Now).Trim()" ;
var urlTeeTimes = "https://localhost:44362/Home/TeeTimes";
var teeTimes = [];
var playerCount = 1;

$(":submit").hide();
//$("#TeeTimes").hide();

$('#datetimepicker1').datetimepicker({
        useCurrent: false,
        minDate: new Date(),
        format: "MM/DD/YYYY"
    })
    .on('dp.change', function (e) {
        $('#datetimepicker1').data("DateTimePicker").hide();
        setTimeout(GetTeeTimes, 500);
        $("#TeeTimeButtons").show();
        $("#TeeTimeButtons").empty();
    });



$('#AddPlayer').on('click', function (e) {

    if (playerCount < GetMaxPlayers()) {
        playerCount = playerCount + 1;
        $('#PlayersInput').val(playerCount);
    }

});


$('#SubtractPlayer').on('click', function (e) {
    if (playerCount > 1) {
        playerCount = playerCount - 1;
        $('#PlayersInput').val(playerCount);
      
    }
});

function GetTeeTimes()
{
    $.get(urlTeeTimes,
        function(data, status) {
            data.forEach(ShowTeeTimes);
            teeTimes = data;
        });
}

function ShowTeeTimes(item, index, arr) {
   
    item.selected = false;
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 2
    });

    var options = {
        hour: 'numeric',
        minute: 'numeric',
        hour12: true
    };

    var templateData = { StartTime: 0, SubTotal: 0, SavingsPercent: 0, FreeSlots: 0, buttonNum: 0 };
    var calendarDate = $('#datetimepicker1').data('date');
    var teeTimeDate = new Date(calendarDate + ' ' + item.StartTime);

    templateData.StartTime = teeTimeDate.toLocaleString('en-US', options);
    templateData.SubTotal = formatter.format(item.SubTotal);
    templateData.SavingsPercent = item.SavingsPercent;
    templateData.FreeSlots = item.FreeSlots;
    templateData.buttonNum = index;

    
    
    // Grab the HTML out of our template tag and pre-compile it.
    var template = _.template(
        $("script.template").html()
    );
    // Define our render data 
    

    // Render the underscore template and inject it into dom    
    $("#TeeTimeButtons").append(
        template(templateData)
    );
}

function OnTeeTime(index, button) {

    //check if allowed to pick more teeTimes
    if (teeTimes[index].selected === false)
        if (GetNumTeeTimesSelected() === GetMaxTeeTimes())
            return;
    

    //highlight selected TeeTimes
    teeTimes[index].selected = !teeTimes[index].selected;    
    $(button).toggleClass("active");

    


    //show hide book button
    if (GetNumTeeTimesSelected() > 0) 
        $(":submit").show();
    else
    
        $(":submit").hide();
}

function GetNumTeeTimesSelected() {
    var teeTimesSelected = 0;
    teeTimes.forEach(function (item) {
            if (item.selected === true)
                teeTimesSelected += 1;
        }
    );
    return teeTimesSelected;
}

function GetMaxPlayers() {
    if (window.isGroupTeeTime === 'True')
        return 12;
    else
        return 4;
}

function GetMaxTeeTimes() {
    if (window.isGroupTeeTime === 'True')
        return 3;
    else
        return 1;
}


$(":submit").on('click',
    function() {

        var index = 0;
        for (var i = 0; i < teeTimes.length; i++)
        {

        
        
            if (teeTimes[i].selected === true)
            {
                var teeTimeHiddenInput = $('<input type="hidden" name="TeeTimes[' +
                    index +
                    '].TeeTime" value="' +
                    teeTimes[i].StartTime +
                    '"/>');
                $("#TeeTimes").append(teeTimeHiddenInput);


                var subTotalHiddenInput = $('<input type="hidden" name="TeeTimes[' +
                    index +
                    '].SubTotal" value="' +
                    teeTimes[i].SubTotal +
                    '"/>');
                $("#TeeTimes").append(subTotalHiddenInput);

                var cartsCountHiddenInput = $('<input type="hidden" name="TeeTimes[' +
                    index +
                    '].CartsCount" value="' +
                    teeTimes[i].CartsCount + 
                    '"/>');
                $("#TeeTimes").append(cartsCountHiddenInput);

                var savingsPercentHiddenInput = $('<input type="hidden" name="TeeTimes[' +
                    index +
                    '].SavingsPercent" value="' +
                    teeTimes[i].SavingsPercent +
                    '"/>');
                $("#TeeTimes").append(savingsPercentHiddenInput);

                index++;
            }
        }
      
    });

