$( document ).ready(function() {
    
    var $form = $('.form-searchcities');
    var $input = $('.form-searchcities #form-searchcities-input');
    var $content = $(".form-searchcities-content");
    var $countrylinks = $(".country-link");
    var $countryresults = $(".country-result");

    /*
        Display content on input focus
    */
    $input.focus(function(e){
        $content.addClass("active");
    });

    $(document).click(function(event) { 

        if(!$(event.target).closest($content).length && !$(event.target).closest($input).length) {

            if($($content).is(":visible")) {
                $content.removeClass("active");
            }
        }        
    })

    /*
        Display content on submit if result is empty
    */
    $form.on("submit", function() {

        if (this.city.value == "") {
            
            $($input).focus();
            return false;
        }

        return true;
    });

    /*
        Toggle country results depending on country links
    */
    $countrylinks.on("click", function (e) {

        var country = this.dataset.country;
        
        $countrylinks.removeClass('active')
        this.classList.add("active");        

        $countryresults.removeClass('active')

        for (var j = 0; j < $countryresults.length; j++) {

            if ($countryresults[j].id == country) {

                $countryresults[j].classList.add("active");
                break;                
            }
        }
    });
});