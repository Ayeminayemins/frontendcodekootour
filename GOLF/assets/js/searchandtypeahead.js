$( document ).ready(function() {
    
    var $form = $('.form-searchcities');
    var $input = $('.form-searchcities #form-searchandtypeahead-input');
    var $content = $(".form-searchcities-content");
    var $countrylinks = $(".country-link");
    var $countryresults = $(".country-result");
    var $close = $('.btn-close');

    /*
        Display content on input focus
    */
    $input.focus(function(e){

        if (this.value == "")
            opencontent();
    });
    
    $input.on("change paste keyup", function(e){
       closecontent();
    });

    $(document).click(function(event) { 

        if(!$(event.target).closest($content).length && !$(event.target).closest($input).length) {
            closecontent();
        }        
    })

    /*
        Display content on submit if result is empty
    */
    $form.on("submit", function() {

        if (this.city.value == "") {
            
            $($input).focus();
            return false;
        }

        return true;
    });

    /*
        Toggle country results depending on country links
    */
    $countrylinks.on("mouseover", function (e) {

        var country = this.dataset.country;
        
        $countrylinks.removeClass('active')
        this.classList.add("active");        

        $countryresults.removeClass('active')

        for (var j = 0; j < $countryresults.length; j++) {

            if ($countryresults[j].id == country) {

                $countryresults[j].classList.add("active");
                break;                
            }
        }
    });

    $input.typeahead(
        {
        hint: true,
        highlight: true,
    } , {
        limit: 10,
        templates: {
           suggestion: function(data) {
                return '<p>' + data.Name + '</p>';
            }
        },
        display: function(item){ return item.Name},
        name: 'tours',
        source: function(query, syncResults, asyncResults) {
            $.get("/home/AutoCompleteSuggestions?maxrows=4&name_startsWith=" + query, function(data) {
              asyncResults(data);
            });
        }
    });

    $input.bind('typeahead:select', function(ev, suggestion) {
        window.location.href = window.location.origin + suggestion.ClubInUrl ;
    });

    $close.on("click", function (E) {

        $input.typeahead('close');
        $input.val('');
    })

    

    var closecontent = function () {
        if($($content).is(":visible")) {
            $content.removeClass("active");
        }
    }

    var opencontent = function () {
        if(!$($content).is(":visible")) {
            $content.addClass("active");
        }
    }    
});