$(function () {
    var toursummary = $("#readmore1 > p")[0];
	var toursummaryreadmore = new ReadMore($("#readmore1"), $(toursummary).height()).init();
    
    var information = $("#readmore2 > table")[0];
    var informationreadmore = new ReadMore($("#readmore2"), $(information).height()).init();

	var widgetcalendar = $('#widget-calendar').datetimepicker({
        minDate: new Date(),
        defaultDate: new Date(),
        format: "MM/DD/YYYY",
        disabledDates: [moment("06/01/2016"), moment("06/02/2016"), moment("06/03/2016")]
    }).on('dp.change', function(e) {
        $('#widget-calendar').data("DateTimePicker").hide();
    });

    var wishlist = new Wishlist();
    wishlist.setLink(document.getElementsByClassName("wishlink"));
});
